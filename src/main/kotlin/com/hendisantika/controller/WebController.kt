package com.hendisantika.controller

import com.hendisantika.model.Customer
import com.hendisantika.repo.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by hendisantika on 6/30/17.
 */
@RestController
class WebController {
    @Autowired
    lateinit var repository: CustomerRepository

    @RequestMapping()
    fun save(): String {
        repository.save(Customer("Jonh", "Snow"))
        repository.save(Customer("Inokentyi", "Bogdanov"))
        repository.save(Customer("Meirambek", "Muratov"))
        return "Done"
    }

    @RequestMapping("/findAll")
    fun findAll() = repository.findAll()

    @RequestMapping("/findById/{id}")
    fun findById(@PathVariable id: Long) = repository.findById(id)
}